package com.medium.medium.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Project Name : medium-auth-service
 * Author : ChinthakaDi
 * Date : 11/8/2018 - 5:26 PM
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Document(collection = "AppUser")
public class AppUser {

    @Id
    private String id;

    @Indexed(unique = true)
    private String username;

    private String password;

    private String role;

    private List<AppUserRole> appUserRoles;

    public AppUser() {
    }

    public AppUser(String id, String username, String password, String role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
    }
}
