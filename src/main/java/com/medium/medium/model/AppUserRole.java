package com.medium.medium.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Project Name : medium-auth-service
 * Author : ChinthakaDi
 * Date : 11/8/2018 - 5:40 PM
 */
@Getter
@Setter
public class AppUserRole {

    @Id
    private String id;

    private String roleName;

    public AppUserRole(String roleName) {
        this.roleName = roleName;
    }
}
