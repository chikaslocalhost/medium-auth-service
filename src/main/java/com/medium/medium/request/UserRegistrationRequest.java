package com.medium.medium.request;

import com.medium.medium.model.AppUserRole;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Project Name : medium-auth-service
 * Author : ChinthakaDi
 * Date : 11/13/2018 - 12:27 PM
 */
@Getter
@Setter
@ToString
public class UserRegistrationRequest {

    private String username;

    private String password;

    private List<AppUserRole> appUserRoles;

}
