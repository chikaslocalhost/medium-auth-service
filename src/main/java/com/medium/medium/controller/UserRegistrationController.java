package com.medium.medium.controller;

import com.medium.medium.model.AppUser;
import com.medium.medium.model.AppUserRole;
import com.medium.medium.repository.AppUserRepository;
import com.medium.medium.request.UserRegistrationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.java.Log;

/**
 * Project Name : medium-auth-service
 * Author : ChinthakaDi
 * Date : 11/13/2018 - 12:26 PM
 */
@Log
@RestController
@RequestMapping(value = "v1/api/user")
public class UserRegistrationController {

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    AppUserRepository appUserRepository;

    @PostMapping(value = "/create")
    public ResponseEntity createUser(@RequestBody UserRegistrationRequest userRegistrationRequest){
        AppUser appUser = new AppUser();
        appUser.setPassword(bCryptPasswordEncoder.encode(userRegistrationRequest.getPassword()));
        appUser.setUsername(userRegistrationRequest.getUsername());
        List<AppUserRole> appUserRoles = new ArrayList<>();
        appUserRoles.add(new AppUserRole("ROLE_ADMIN"));
        appUserRoles.add(new AppUserRole("ROLE_NEWS"));
        appUser.setAppUserRoles(appUserRoles);
        appUserRepository.save(appUser);
        return ResponseEntity.ok("User Registration Successfully Completed");
    }
}
