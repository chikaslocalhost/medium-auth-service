package com.medium.medium.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

/**
 * Project Name : medium-api-gateway
 * Author : ChinthakaDi
 * Date : 11/4/2018 - 1:52 AM
 */
@Getter
@Setter
public class JwtConfig {

    @Value("${security.jwt.uri:/auth/**}")
    private String Uri;

    @Value("${security.jwt.uri:/app/login}")
    private String authUrl;

    @Value("${security.jwt.header:Authorization}")
    private String header;

    @Value("${security.jwt.prefix:Bearer }")
    private String prefix;

    @Value("${security.jwt.expiration:#{24*60*60}}")
    private int expiration;

    @Value("${security.jwt.secret:medium_api_secret}")
    private String secret;

}
