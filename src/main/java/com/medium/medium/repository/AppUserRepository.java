package com.medium.medium.repository;

import com.medium.medium.model.AppUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Project Name : medium-auth-service
 * Author : ChinthakaDi
 * Date : 11/8/2018 - 5:49 PM
 */
@Repository
public interface AppUserRepository extends MongoRepository<AppUser, String> {

    AppUser findAppUserByUsername(String username);

}
