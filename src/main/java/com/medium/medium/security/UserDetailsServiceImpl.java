package com.medium.medium.security;

import com.medium.medium.model.AppUser;
import com.medium.medium.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Project Name : medium-auth-service
 * Author : ChinthakaDi
 * Date : 11/4/2018 - 2:22 AM
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private AppUserRepository appUserRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        AppUser appUserByUsername = appUserRepository.findAppUserByUsername(username);
        if(appUserByUsername != null){
            // Remember that Spring needs roles to be in this format: "ROLE_" + userRole (i.e. "ROLE_ADMIN")
            // So, we need to set it to that format, so we can verify and compare roles (i.e. hasRole("ADMIN")).

            Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            appUserByUsername.getAppUserRoles().stream().forEach(appUserRole -> {
                GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(appUserRole.getRoleName());
                grantedAuthorities.add(grantedAuthority);
            });

            // The "User" class is provided by Spring and represents a model class for user to be returned by UserDetailsService
            // And used by auth manager to verify and check user authentication.
            return new User(appUserByUsername.getUsername(), appUserByUsername.getPassword(), grantedAuthorities);
        }
        // If user not found. Throw this exception.
        throw new UsernameNotFoundException("Username: " + username + " not found");
    }


}

